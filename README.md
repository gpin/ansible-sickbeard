# Ansible SickBeard
Install and configure SickBeard on CentOS/RHEL or Ubuntu/Debian.

### SickBeard configuration
```
sickbeard_dir: /var/lib/sickbeard 									# sickbeard installation directory
sickbeard_port: 8081 												# sickbeard port
sickbeard_user: sickbeard 											# sickbeard user
sickbeard_group: sickbeard 											# sickbeard group
sickbeard_repo: git://github.com/midgetspy/Sick-Beard.git 			# sickbeard repo
sickbeard_piddir: /var/run/sickbeard 								# sickbeard piddir
sickbeard_pidfile: '{{ sickbeard_piddir }}/sickbeard.pid' 			# sickbeard pidfile
```

## Dependencies
None.

## Example Playbook
    - hosts: whatever
      become: yes
      roles:
        - sickbeard